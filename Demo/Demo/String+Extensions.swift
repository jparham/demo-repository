//
//  String+Extensions.swift
//  Demo
//
//  Created by Jonathan Parham on 8/20/20.
//  Copyright © 2020 Jonathan Parham. All rights reserved.
//

import UIKit

extension String {
    func doStuff() {
        print("Hello, World!")
    }
}
